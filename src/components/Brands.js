import React, { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Dropdown from './Dropdown';
import useHttp from '../hooks/http.hook';
import { baseURL } from '../shared/endpoint';
import '../styles/Dropdown.css';

const Brands = ({ value }) => {
    const { open, toggle } = useHttp();
    const [ brands, setBrands ] = useState(null);
    const history = useHistory();
    const location = useLocation();

    const showList = async () => {
        const response = await toggle(`${baseURL}brands_terms`);
        response ?
            setBrands(response.data) :
            setBrands(null);
    }

    const chooseOption = event => {
        history.push(`${location.pathname}/b-${event.target.value}`);
    }
    
    return (
        <Dropdown 
            title='Brands'
            status={open}
            values={brands}
            value={value}
            click={showList} 
            change={event => chooseOption(event)} 
        />     
    )
}

export default Brands;