import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import DropdownList from './DropdownList';
import '../styles/App.css';

const App = () => (
    <Router>
        <Switch>
            <Route path='/' component={DropdownList} />
            <Route path='/:s/:b/:st/:id/' component={DropdownList} />
        </Switch>
    </Router>
)

export default App;