import React from 'react';

const Mapped = ({ values }) => 
    values && values.map(value => (
        <option key={value.id} value={value.slug}>{value.label}</option>  
    ))

export default Mapped;