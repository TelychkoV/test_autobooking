import React from 'react';
import Mapped from './Mapped';
import '../styles/Dropdown.css';

const Dropdown = ({ click, change, title, status, values, value }) => (
    <select onClick={click} onChange={change} className='select' value={value}>
        <option>{value || title}</option>
        {status ? <Mapped values={values} /> : null}
    </select>
)

export default Dropdown;