import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import Dropdown from './Dropdown';
import useHttp from '../hooks/http.hook';
import { baseURL } from '../shared/endpoint';
import '../styles/Dropdown.css';

const Services = ({ value }) => {    
    const { open, toggle } = useHttp();
    const [ services, setServices ] = useState(null);
    const history = useHistory();

    const showList = async () => {
        const response = await toggle(`${baseURL}terms`);
        response ?
            setServices(response.data) :
            setServices(null)
    }

    const chooseOption = event => {
        history.push(`s-${event.target.value}`)
    }
    
    return (
        <Dropdown 
            title='Services' 
            status={open}
            value={value}
            values={services}
            click={showList} 
            change={event => chooseOption(event)} 
        />
    )
}

export default Services;