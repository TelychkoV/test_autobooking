import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import Services from './Services';
import Brands from './Brands';
import Styles from './Styles';
import useHttp from '../hooks/http.hook';
import { baseURL } from '../shared/endpoint';
import '../styles/App.css';

const DropdownList = () => {
    const { request } = useHttp();
    const location = useLocation();
    const [ data, setData ] = useState({});

    useEffect(() => {
        if(location.pathname.length > 1) {
            const parsed = location.pathname.split('/');   
            const [ _, service, brand, style] = parsed.map(value => {
                if(value.startsWith('s-') || value.startsWith('b-')) {
                    value = value.substr(2);
                }

                if(value.startsWith('st-')) {
                    value = value.substr(3);
                }
                return value;
            })     
            request(`${baseURL}parse_link?service_slug=${service}&brand_slug=${brand}&style_slug=${style}`)
                .then(response => setData(obj => ({ ...obj, ...response })))
        }
    }, [request, location.pathname])

    const setDefaultData = entity => {
        if(Object.keys(data).length) {
            return data[entity].label;
        }
    }

    return (
        <section className='container'>
            <section className='wrapper'>
                <Services value={setDefaultData('service')} />
                <Brands value={setDefaultData('brand')} />
                <Styles value={setDefaultData('style')}  />
            </section>  
        </section>
    )
}

export default DropdownList;