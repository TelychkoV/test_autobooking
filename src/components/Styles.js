import React, { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Dropdown from './Dropdown';
import useHttp from '../hooks/http.hook';
import { baseURL } from '../shared/endpoint';
import '../styles/Dropdown.css';

const Styles = ({ value }) => {
    const { open, toggle } = useHttp();
    const [ styles, setStyles ] = useState(null);
    const history = useHistory();
    const location = useLocation();

    const showList = async () => {
        const response = await toggle(`${baseURL}styles`);
        response ?
            setStyles(response.data) :
            setStyles(null);
    }
    
    const chooseOption = event => {
        history.push(`${location.pathname}/st-${event.target.value}`);
    }
    
    return (
        <Dropdown 
            title='Styles'
            status={open}
            values={styles}
            value={value}
            click={showList} 
            change={event => chooseOption(event)} 
        /> 
    )
}

export default Styles;