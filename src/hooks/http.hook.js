import { useState, useCallback } from 'react';

const useHttp = () => {
    const [open, setOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const request = useCallback(async (url, method = 'GET', body = null, headers = {}) => {
        setLoading(true);
        try {
            const response = await fetch(url, { method, body, headers });
            const data = await response.json();

            setLoading(false);
            
            return data;

        } catch (error) {
            setLoading(false);
            setError(error.message);
            throw error;
        }
    }, []);

    const toggle = (url) => {
        setOpen(!open);
        if(!open) {
            return request(url);
        }

        return null;;
    }

    return { request, loading, error, open, toggle };
};

export default useHttp;
